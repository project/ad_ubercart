<?php

/**
 * @file
 * Provides all needed forms' alterations.
 */

define('QUOTA_EXCEED_USED_DATES', 1);
define('QUOTA_EXCEED_MAX_SELECTED', 2);

/**
 * Implementation of hook_form_alter().
 */
function ad_ubercart_form_alter(&$form, &$form_state, $form_id) {
  // Adding ad plans and date pickers to "Add to cart" form
  if (strpos($form_id, 'add_to_cart_form') || strpos($form_id, 'add_product_form')) {
    ad_ubercart_add_to_cart_form_alter($form, $form_state);
  }
  if (strpos($form_id, 'ad_product') === 0) {
    ad_ubercart_form_ad_product_node_form_alter($form, $form_state);
  }
  if ($form_id === AD_CONTENT_TYPE .'_node_form') {
    if (!user_access('administer advertisements')) {
      unset($form['schedule']);
      unset($form['adstatus']);
    }
  }
}

/**
 * Implementation of hook_form_$form_id_alter().
 *
 * Removing useless things from product form.
 */
function ad_ubercart_form_ad_product_node_form_alter(&$form, &$form_state) {
  $node = $form['#node'];
  $form['base']['shippable']['#default_value'] = FALSE;
  $form['base']['shippable']['#type'] = 'hidden';
  unset($form['base']['weight']);
  unset($form['base']['dimensions']);
  unset($form['base']['pkg_qty']);
  unset($form['base']['default_qty']);
  unset($form['base']['ordering']);
  
  $form['base']['prices']['cost']['#title'] = t('Cost per day');
  $form['base']['prices']['cost']['#description'] = t('This will be used to calculate a refund amount.');
}

/**
 * Adding Ad plans and calendar to cart adding form.
 */
function ad_ubercart_add_to_cart_form_alter(&$form, &$form_state) {
  $product = node_load($form['nid']['#value']);
  if (strpos($product->type, 'ad_product') === 0) {
    if (empty($form_state['storage'])) {
      add_to_cart_first_step_form($form, $form_state, $product);
    }
    else {
      add_to_cart_second_step_form($form, $form_state, $product);
    }
  }
}

/**
 * Add to cart: first step
 */
function add_to_cart_first_step_form(&$form, &$form_state, $product) {
  $form['ad_plan'] = array(
    '#type' => 'value',
    '#value' => $product->type,
  );

  if ($product->type == 'ad_product_daily') {
    // Qty should not be editable manually
    $form['qty']['#type'] = 'hidden';

    // Get all daily quota
    $daily_quota = get_quota('daily');

    // this will produce date in format YYYY-MM-DD from first array item
    reset($daily_quota);
    $startDate = key($daily_quota) .'-01';

    // this will produce date in format YYYY-MM-DD from last array item
    end($daily_quota);
    list($y, $m) = explode('-', key($daily_quota));
    $endDate = date('Y-m-d', mktime(0, 0, 0, $m + 1, 0, $y));

    // Remove used quota from calendar
    $used_quota = get_used_quota('daily');
    foreach ($used_quota as $date) {
      // this will produce YYYY-MM from date
      $month = substr($date, 0, 7);
      if (isset($daily_quota[$month])) {
        $daily_quota[$month]--;
      }
    }

    // Hatch already added dates
    $in_cart = array();
    if (isset($form['#order_id'])) {
      // Look into opened order products
      $order = uc_order_load($form['#order_id']);
      $items = $order->products;
    }
    else {
      // Look into user's cart and mark down items which are already selected
      $items = uc_cart_get_contents();
    }
    foreach ($items as $item) {
      if ($item->data['ad_plan'] == $product->type) {
        foreach ($item->data['period'] as $key => $period) {
          $in_cart[$key] = $period;
          // we may only add description if something is really present in a cart
          $description = t('<b>Note:</b> Hatched dates is already in your cart. If you select them, they will be removed from your old selections.');
        }
      }
    }

    $available = FALSE;
    foreach ($daily_quota as $quota) {
      if ($quota) {
        $available = TRUE;
        break;
      }
    }

    if ($available) {
      $form['period'] = array(
        '#type' => 'date_picker',
        '#title' => t('Period'),
        '#description' => isset($description) ? $description : '',
        '#multiple' => TRUE,
        '#calendar_attributes' => array(
          'inline' => TRUE,
          'selectMultiple' => TRUE,
          'endDate' => $endDate,
          'sartDate' => $startDate,
          'monthsToDisplay' => min(count($daily_quota), 3),
          'maxSelected' => array('by_month' => $daily_quota),
          'disabled_dates' => $used_quota,
          'in_cart' => $in_cart,
        ),
        '#attributes' => array('price' => $product->sell_price),
        '#suffix' => '<p class="price-holder"></p>',
      );
    }
    else {
      $form['period'] = array(
        '#value' => '<p>'. t('Sorry, there are no available quota.') .'</p>',
      );
    }
  }
  elseif ($product->type == 'ad_product_monthly') {

    // Get all daily quota
    $monthly_quota = get_available_quota('monthly');
    // Generate options for a month select box.
    if (!empty($monthly_quota)) {
      foreach ($monthly_quota as $month => $quota) {
        $options[$month] = ad_ubercart_format_month($month);
      }
    }
    
    // Look into user's cart and mark down items which are already selected
    if (isset($form['#order_id'])) {
      // Look into opened order products
      $order = uc_order_load($form['#order_id']);
      $items = $order->products;
    }
    else {
      // Look into user's cart and mark down items which are already selected
      $items = uc_cart_get_contents();
    }
    foreach ($items as $item) {
      if ($item->data['ad_plan'] == $product->type) {
        foreach ($item->data['period'] as $key => $period) {
          if (isset($options[$key])) {
            $options[$key] .= '*';
            // We may only add description if something is really present in a cart
            $description = t('<b>Note:</b> * — this item is already in your cart. If you select them, they will be removed from your old selections.');
          }
        }
      }
    }
    
    $available = !empty($options);
    
    if ($available) {
      $form['period'] = array(
        '#type' => 'select',
        '#title' => t('Period'),
        '#description' => $description,
        '#options' => $options,
        '#multiple' => TRUE,
        '#attributes' => array('price' => $product->sell_price),
        '#suffix' => '<p class="price-holder"></p>',
      );
    }
    else {
      $form['period'] = array(
        '#value' => '<p>'. t('Sorry, there are no available quota.') .'</p>',
      );
    }
  }
  
  // Handlers setup

  // Add to Cart form
  if (strpos($form['form_id']['#value'], 'uc_product_add_to_cart_form') !== FALSE) {
    if ($available) {
      $form['submit']['#weight'] = 100;
      $form['#validate'][] = 'ad_ubercart_add_to_cart_validate';
    }
    else {
      unset($form['submit']);
    }
  }
  // Order administration form
  else {
    // Ad node selector for order administration form
    $form['existing_node'] = array(
      '#type' => 'textfield',
      '#title' => t('Linked Advertisement\'s NID'),
      '#autocomplete_path' => 'ad/autocomplete',
      '#description' => t('You may search advertisement by typing <em>##</em> and first chars of ad\'s NID or <em>::</em> and piece of ad\'s title.')
    );

    $form['buttons']['submit']['#validate'][] = 'ad_ubercart_add_to_cart_validate';
    array_unshift($form['buttons']['submit']['#submit'], 'ad_ubercart_add_to_cart_submit');
  }
}

/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for advertisiment nodes.
 */
function ad_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    // Allow searching by nid, just by typing "#nid"
    $result = db_query_range("SELECT n.nid, CONCAT('##', n.nid, '::', n.title) as \"title\" FROM {node} n WHERE n.type = '%s' AND CONCAT('##', n.nid, '::', LOWER(n.title)) LIKE LOWER('%%%s%%')", AD_CONTENT_TYPE, $string, 0, 10);
    while ($node = db_fetch_object($result)) {
      $matches[$node->nid] = $node->title;
    }
  }

  drupal_json($matches);
}


/**
 * "Add to cart: first step" validator
 */
function ad_ubercart_add_to_cart_validate(&$form, &$form_state) {
  if (!isset($form_state['values']['qty'])) {
    form_set_error('qty', t('Qty should be greater zero.'));
  }
  $check_cart_contents = $form_state['values']['form_id'] != 'uc_order_edit_form';
  if ($form_state['values']['ad_plan'] == 'ad_product_daily') {
    // Check if any date selected
    if (!isset($form_state['values']['period']) || empty($form_state['values']['period'])) {
      form_set_error('period', t('Please, fill the period.'));
    }
    else {
      $period = drupal_map_assoc(explode(',', $form_state['values']['period']));
      validate_period($period, 'daily', $check_cart_contents);
    }
  }
  elseif ($form_state['values']['ad_plan'] == 'ad_product_monthly') {
    // Check if any month selected
    if (!isset($form_state['values']['period']) || empty($form_state['values']['period'])) {
      form_set_error('period', t('Please, fill the period.'));
    }
    else {
      $period = $form_state['values']['period'];
      validate_period($period, 'monthly', $check_cart_contents);
    }
  }

  $errors = form_get_errors();
  if (empty($errors) && ((strpos($form_state['values']['form_id'], 'uc_product_add_to_cart_form') !== FALSE) && empty($form_state['storage']))) {
    $form_state['storage'] = $form_state['values'];
    unset($form_state['values']);
    $form_state['rebuild'] = true;
  }
}

function validate_period($period, $type, $check_cart_contents) {
  // Check quota amount
  $result = is_exceed_quota($type, $period);
  if ($result) {
    form_set_error('period', t('You have exceeded available quota, please, check your selection.'));
  }
  if ($check_cart_contents) {
    // Remove duplicates from a cart.
    $items = uc_cart_get_contents();
    foreach ($items as $item) {
      if ($item->data['ad_plan'] == 'ad_product_'. $type) {
        $period = array_merge($period, $item->data['period']);
      }
    }
    $result = is_exceed_quota($type, $period);
    if ($result) {
      form_set_error('period', t('The sum of your cart items and current selection is exceeding available quota. Please, remove some of your cart items, to get free slots and than, try to submit the form again.'));
    }
  }
}

/**
 * Add to cart: second step
 */
function add_to_cart_second_step_form(&$form, &$form_state, $product) {
  global $user;
  drupal_add_js('misc/collapse.js');

  // Cleanup form from first step
  unset($form['nid']);
  // Also get rid of useless handlers from first step
  $form['#fs_submit'] = $form['#submit'];
  $form['#fs_validate'] = $form['#validate'];
  unset($form['#submit']);
  unset($form['#validate']);
  // Get rid of useless buttons
  unset($form['submit']);

  // Give ability for regitered users to choose existing ads
  if ($user->uid) {
    $existing_ads = db_result(db_query('SELECT COUNT(*) FROM {node} n WHERE n.type = "%s" AND n.uid = %d', AD_CONTENT_TYPE, $user->uid));

    // ...but only if they have ads
    if ($existing_ads) {
      $form['adnode_wrapper']['adnode'] = array(
        '#type' => 'select',
        '#title' => t('Advertisement'),
        '#options' => array(
          0 => t('Please, choose'),
          1 => t('Existing...'),
          2 => t('Create new...'),
        ),
        '#default_value' => $form_state['values']['adnode'],
        '#ahah' => array(
          'path' => 'cart/ad_ubercart/ahah',
          'wrapper' => 'ad-ubercart-wrapper',
          'event' => 'change',
        ),
      );
      $form['adnode_wrapper']['adnode_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Select',
        '#suffix' => '<script type="text/javascript">$(\'#edit-adnode-submit\').css(\'display\', \'none\');</script>',
      );
    }
  }

  if (!isset($form['adnode_wrapper']['adnode']) || $form_state['values']['adnode'] == 2) {
    $form['node'] = add_to_cart_second_step_form_create_node($form, $form_state);
    // TODO: FileField: Ease integration
    // Override default FileField's AHAH paths.
    ad_ubercart_overide_filefield_ahah_path($form['node'], array('node'));
  }
  elseif ($form_state['values']['adnode'] == 1) {
    $form['node'] = add_to_cart_second_step_form_existing_node($form, $form_state);
  }
  
  if (isset($form['node'])) {
    $form['node']['#prefix'] = '<div id="ad-ubercart-wrapper">';
    $form['node']['#suffix'] = '</div>';
  }
  else {
    $form['node'] = array(
      '#value' => '<div id="ad-ubercart-wrapper"></div>',
    );
  }
}

/**
 * Add to cart: second step: AHAH callback
 */
function ad_ubercart_add_to_cart_ahah() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  switch ($_POST['adnode']) {
    case 1:
      $form['node'] = add_to_cart_second_step_form_existing_node($form, $form_state);
      break;
    case 2:
      $form['node'] = add_to_cart_second_step_form_create_node($form, $form_state);
      // TODO: FileField: Ease integration
      // Override default FileField's AHAH paths.
      ad_ubercart_overide_filefield_ahah_path($form['node'], array('node'));
      break;
  }

  form_set_cache($form_build_id, $form, $form_state);

  // Sanity check for form_builder values.
  if (!empty($form_state['values'])) {
    $_POST = array_merge($_POST, $form_state['values']);
  }

  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );

  // Rebuild and render the form.
  $form = form_builder($_POST['form_id'], $form, $form_state);
  $output = drupal_render($form['node']);
  if ($output) {
    $javascript = drupal_add_js(NULL, NULL);
    if (isset($javascript['setting'])) {
      $output .= '<script type="text/javascript">jQuery.extend(Drupal.settings, '. drupal_to_js(call_user_func_array('array_merge_recursive', $javascript['setting'])) .');Drupal.attachBehaviors("form");</script>';
    }

    drupal_json(array(
      'status'   => TRUE,
      'data'     => $output,
    ));
  }
}

/**
 * Add to cart: second step: Prepare existing Ad selection form
 */
function add_to_cart_second_step_form_existing_node($form, $form_state) {
  global $user;

  $existing_ads = array();
  $result = db_query_range('SELECT nid FROM {node} WHERE type = "%s" AND uid = %d ORDER BY created DESC', AD_CONTENT_TYPE, $user->uid, 0, 10);
  while ($nid = db_result($result)) {
    $ad = node_load($nid);
    $existing_ads[$nid] = theme('ad_in_cart', $ad);
  }

  $ad_form['existing_node'] = array(
    '#type' => 'radios',
    '#title' => t('Your Advertisements'),
    '#options' => $existing_ads,
    '#default_value' => $form_state['values']['existing_node'],
  );
  $ad_form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#validate' => $form['#fs_validate'],
    '#submit' => array_merge(array('ad_ubercart_add_to_cart_submit'), $form['#fs_submit']),
  );
  return $ad_form;
}

/**
 * Add to cart: second step: Prepare Ad creation form
 */
function add_to_cart_second_step_form_create_node($form, &$form_state) {
  global $user;
  module_load_include('pages.inc', 'node');

  // Create new node form
  $node = new stdClass();
  $node->type = AD_CONTENT_TYPE;
  if ($user->uid) {
    $node->uid = $user->uid;
    $node->name = $user->name;
  }

  // Prefill other values from content type settings.
  node_object_prepare($node);
  if (empty($form_state['values'])) {
    $form_state['values'] = array();
  }
  $form_state['values'] = array_merge($form_state['values'], (array)$node);

  // Retrieve and prepare a node form.
  $ad_form = drupal_retrieve_form($node->type .'_node_form', $form_state, $node);
  drupal_prepare_form($node->type .'_node_form', $ad_form, $form_state);

  // Strip out teaser field, as it useless and brings validation warnings.
  // This code taken from Excerpt module.
  if(isset($ad_form['body_field']['teaser_js'])) {
    unset($ad_form['body_field']['teaser_js']);
    unset($ad_form['body_field']['teaser_include']);

    if(!empty($ad_form['body_field']['#after_build'])) {
      if($id = array_search('node_teaser_js', $ad_form['body_field']['#after_build'])) {
        unset($ad_form['body_field']['#after_build'][$id]);
      }
      if($id = array_search('node_teaser_include_verify', $ad_form['body_field']['#after_build'])) {
        unset($ad_form['body_field']['#after_build'][$id]);
      }
    }
  }

  // Cut form's identity
  unset($ad_form['form_id']);
  unset($ad_form['form_token']);
  $ad_form['#type'] = 'markup';

  // TODO: FileField: Ease integration
  // Override default FileField's validator.
  foreach ($ad_form['#validate'] as $key => $validator) {
    if ($validator == 'filefield_node_form_validate') {
      $ad_form['#validate'][$key] = 'ad_ubercart_filefield_node_form_validate';
    }
  }

  // Handlers setup
  if (strpos($form['form_id']['#value'], 'uc_product_add_to_cart_form') !== FALSE) {
    array_unshift($ad_form['#validate'], 'ad_ubercart_add_to_cart_second_step_validate');
    $ad_form['buttons']['preview']['#validate'] = array_merge($ad_form['#validate'], $form['#fs_validate']);
    $ad_form['buttons']['submit']['#validate'] = $ad_form['buttons']['preview']['#validate'];
    
    $ad_form['buttons']['submit']['#submit'][] = 'ad_ubercart_add_to_cart_submit';
    $ad_form['buttons']['submit']['#submit'] = array_merge($ad_form['buttons']['submit']['#submit'], $form['#fs_submit']);

    unset($ad_form['buttons']['delete']);
    unset($ad_form['buttons']['preview']);
  }
  
  return $ad_form;
}


/**
 * "Add to cart: second step" validator
 */
function ad_ubercart_add_to_cart_second_step_validate($form, &$form_state) {
  // Include Node module to get it's validator's implementations.
  module_load_include('pages.inc', 'node');
}

/**
 * "Add to cart" submit handler.
 */
function ad_ubercart_add_to_cart_submit($form, &$form_state) {
  // Restore values from first step
  if (!empty($form_state['storage'])) {
    $form_state['values'] = array_merge($form_state['values'], $form_state['storage']);
    unset($form_state['storage']);
  }

  // Add Advertisiment node to cart contents. 
  if ($form_state['values']['existing_node']) {
    $form_state['values']['ad'] = ad_ubercart_clone_ad($form_state['values']['existing_node']);
  }
  else {
    // By some weird reasons, correct nid is placing to the
    // root of $forms_state array after new node creation.
    $form_state['values']['ad'] = $form_state['nid'];
  }

  // Update quantity according to selected period
  if ($form_state['values']['ad_plan'] == 'ad_product_daily') {
    $form_state['values']['period'] = drupal_map_assoc(explode(',', $form_state['values']['period']));
    $form_state['values']['qty'] = count($form_state['values']['period']);
  }
  elseif ($form_state['values']['ad_plan'] == 'ad_product_monthly') {
    $form_state['values']['qty'] = count($form_state['values']['period']);
  }
  
  // Remove duplicates from a cart/order
  if ($form_state['values']['form_id'] != 'uc_order_edit_form') {
    remove_duplicates_from_cart($form_state['values']);
  }
  else {
    remove_duplicates_from_order($form['#order_id'], $form_state['values']);
  }

  /************************************************************************
   *** Here should be added additional submit handlers of the cart contents
   ***/
}

/**
 * Clone existing node.
 */
function ad_ubercart_clone_ad($aid) {
  global $user;
  // Create a new node
  $form_state = array();
  module_load_include('inc', 'node', 'node.pages');

  $form_state['values'] = (array)node_load($aid);
  if ($user->uid) {
    $form_state['values']['uid'] = $user->uid;
    $form_state['values']['name'] = $user->name;
  }
  $form_state['values']['op'] = t('Save');
  unset($form_state['values']['nid']);
  $node = array('type' => AD_CONTENT_TYPE);
  drupal_execute(AD_CONTENT_TYPE .'_node_form', $form_state, (object)$node);

  return $form_state['nid'];
}

/**
 * Remove duplicate items from cart.
 */
function remove_duplicates_from_cart($values) {
  $items = uc_cart_get_contents();
  foreach ($items as $item) {
    if ($item->data['ad_plan'] == $values['ad_plan']) {
      $diff = array_intersect_key($item->data['period'], $values['period']);
      if (!empty($diff)) {
        $old_data = $item->data;
        $item->data['period'] = array_diff_key($item->data['period'], $diff);

        $cid = uc_cart_get_id();
        if (empty($item->data['period'])) {
          uc_cart_remove_item($item->nid, $cid, $old_data);

          // Remove linked node as well if it's temporary
          if ($nid = $item->data['ad'] && $ad = node_load($nid) && $ad->uid == 0) {
            node_delete($nid);
          }
        }
        else {
          $item->qty = count($item->data['period']);
          db_query("UPDATE {uc_cart_products} SET qty = %d, changed = %d, data = '%s' WHERE nid = %d AND cart_id = '%s' AND data = '%s'", $item->qty, time(), serialize($item->data), $item->nid, $cid, serialize($old_data));
          cache_clear_all();
        }
        drupal_set_message(t('Your previously added items were updated. Removed !periods.', array('!periods' => implode(', ', $diff))));
      }
    }
  }
}

/**
 * Remove duplicate items from order contents.
 */
function remove_duplicates_from_order($order_id, $values) {
  $order = uc_order_load($order_id);
  foreach ($order->products as $product) {
    if ($product->data['ad_plan'] == $values['ad_plan']) {
      $diff = array_intersect_key($product->data['period'], $values['period']);
      if (!empty($diff)) {
        $product->data['period'] = array_diff_key($product->data['period'], $diff);
        // Remove product from a list if it's period exhaused
        if (empty($product->data['period'])) {
          db_query("DELETE FROM {uc_order_products} WHERE order_product_id = %d", $product->order_product_id);
          if (variable_get('uc_order_logging', TRUE)) {
            uc_order_log_changes($form['#order_id'], array('add' => 'Product removed from order.'));
          }
          // Remove linked node as well if it's temporary
          if ($nid = $product->data['ad'] && $ad = node_load($nid) && $ad->uid == 0) {
            node_delete($nid);
          }
        }
        // or just update it's quantity
        else {
          $product->qty = count($product->data['period']);
          uc_order_product_save($form['#order_id'], $product);
          if (variable_get('uc_order_logging', TRUE)) {
            uc_order_log_changes($form['#order_id'], array('add' => 'Order products were updated.'));
          }
        }
        drupal_set_message(t('Order products were updated. Removed !periods from old products.', array('!periods' => implode(', ', $diff))));
      }
    }
  }
}


/**
 * Implementation of hook_form_$form_id_alter().
 *
 * Hide quantity from cart form.
 */
function ad_ubercart_form_uc_cart_view_form_alter(&$form, &$form_state) {
  $only_ads_in_cart = TRUE;
 foreach (element_children($form['items']) as $item) {
    if ($form['items'][$item]['nid']) {
      // determine if a row is ad
      $data = unserialize($form['items'][$item]['data']['#value']);
      if (isset($data['ad_plan'])) {
        $form['items'][$item]['qty']['#type'] = 'hidden';
      }
      else {
        // if not only ads in a cart, we should live entire column
        $only_ads_in_cart = FALSE;
      }
    }
  }
  // if not only ads in a cart, we should live entire column
  if ($only_ads_in_cart) {
    //unset($form['items']['#columns']['qty']);
  }
}

/**
 * Implementation of hook_form_$form_id_alter().
 *
 * Output ad_product item's information to admin order form.
 */
function ad_ubercart_form_uc_order_edit_products_form_alter(&$form, &$form_state) {
  foreach (element_children($form['products']) as $key) {
    $item = new stdClass();
    $item->data = unserialize($form['products'][$key]['data']['#value']);
    $description = ad_ubercart_cart_item_description($item);
    $form['products'][$key]['title']['#suffix'] = '<div id="prod_title_'. $key .'">'.$description.'</div>';
  }
}

/**
 * Gets all quota information.
 *
 * @param $type
 *   "daily" or "monthly"
 */
function get_quota($type = NULL) {
  $result = db_query('SELECT * FROM {ad_quota} aq WHERE month >= "%s" ORDER BY aq.month ASC', date('Y-m'));
  while ($data = db_fetch_array($result)) {
    if ($type) {
      if ($data[$type]) {
        $quota[$data['month']] = $data[$type];
      }
    }
    else {
      $quota[$data['month']] = $data;
    }
  }
  return $quota;
}

/**
 * Gets used quota information.
 *
 * @param $type
 *   "daily" or "monthly"
 */
function get_used_quota($type) {
  $result = db_query('SELECT * FROM {ad_used_quota} auq WHERE auq.type = "%s" AND date >= "%s"', $type, date('Y-m'));
  $used_quota = array();
  while ($data = db_fetch_array($result)) {
    $used_quota[$data['date']] = $data['date'];
  }
  return $used_quota;
}

/**
 * Gets available quota.
 *
 * @param $type
 *   "daily" or "monthly"
 */
function get_available_quota($type, $additional_period = NULL) {
  // Get all daily quota
  $avail_quota = get_quota($type);
  $used_quota = get_used_quota($type);
  foreach ($used_quota as $date => $d) {
    // this will produce YYYY-MM from date
    $month = substr($date, 0, 7);
    $avail_quota[$month]--;
    if ($avail_quota[$month] == 0) {
      unset($avail_quota[$month]);
    }
  }
  if (!empty($additional_period)) {
    foreach ($additional_period as $date => $d) {
      // avoid double checking
      if (!isset($used_quota[$date])) {
        $used_quota[$date] = $d;
        // this will produce YYYY-MM from date
        $month = substr($date, 0, 7);
        $avail_quota[$month]--;
      }
    }
  }
  return $avail_quota;
}

/**
 * Check quota exceeding. Returns FALSE, if quota was exceeded.
 */
function is_exceed_quota($type, $period) {
  if ($type == 'daily') {
    $used_quota = get_used_quota('daily');
    foreach ($period as $date => $value) {
      if (isset($used_quota[$date])) {
        return QUOTA_EXCEED_USED_DATES;
      }
    }
  }

  $quota = get_available_quota($type, $period);
  foreach ($quota as $date => $value) {
    if ($value < 0) {
      return QUOTA_EXCEED_MAX_SELECTED;
    }
  }
  return FALSE;
}















// TODO: FileField: Ease integration

/**
 * Override default FileField's AHAH paths.
 */
function ad_ubercart_overide_filefield_ahah_path(&$form, $parents) {
  if (isset($form['#field_info'])) {
    foreach ($form['#field_info'] as $field_name => $field) {
      if ($form['#field_info'][$field_name]['type'] == 'filefield') {
        $form[$field_name]['#after_build'] = array('ad_ubercart_filefield_widget_after_build');
        $form[$field_name]['#upline'] = $parents;
      }
    }
  }
}

/**
 * Afterbuild callback which adds parental information for FileField in sub-forms.
 */
function ad_ubercart_filefield_widget_after_build($element, $form_state) {
  foreach (element_children($element) as $key) {
    // Change AHAH submit path
    $element[$key]['filefield_upload']['#ahah']['path'] .= '/'. implode('][', $element['#upline']);
    $element[$key]['filefield_remove']['#ahah']['path'] .= '/'. implode('][', $element['#upline']);

    // Override currently set JS settings for both elements.
    $element[$key]['filefield_upload']['#id'] .= '_changed';
    form_expand_ahah($element[$key]['filefield_upload']);
    $element[$key]['filefield_remove']['#id'] .= '_changed';
    form_expand_ahah($element[$key]['filefield_remove']);
  }
  return $element;
}

/**
 * Override default filefield's validator to validate only subform.
 */
function ad_ubercart_filefield_node_form_validate($form, &$form_state) {
  return filefield_node_form_validate($form['node'], &$form_state);
}

/**
 * Alternative FileField AHAH callback which supports sub-form elements.
 *
 * The only difference with original function is $form = $form[$parents];
 */
function ad_ubercart_filefield_js($type_name, $field_name, $delta) {
  $field = content_fields($field_name, $type_name);

  if (empty($field) || empty($_POST['form_build_id'])) {
    // Invalid request.
    drupal_set_message(t('An unrecoverable error occurred. The uploaded file likely exceeded the maximum file size (@size) that this server supports.', array('@size' => format_size(file_upload_max_size()))), 'error');
    print drupal_to_js(array('data' => theme('status_messages')));
    exit;
  }

  // Build the new form.
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);

  if (!$form) {
    // Invalid form_build_id.
    drupal_set_message(t('An unrecoverable error occurred. This form was missing from the server cache. Try reloading the page and submitting again.'), 'error');
    print drupal_to_js(array('data' => theme('status_messages')));
    exit;
  }

  // Build the form. This calls the file field's #value_callback function and
  // saves the uploaded file. Since this form is already marked as cached
  // (the #cache property is TRUE), the cache is updated automatically and we
  // don't need to call form_set_cache().
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form['#post'] = $_POST;
  $form = form_builder($form_id, $form, $form_state);

  $parents = arg(5);
  if (isset($parents)) {
    $form = $form[$parents];
  }

  // Update the cached form with the new element at the right place in the form.
  if (module_exists('fieldgroup') && ($group_name = _fieldgroup_field_get_group($type_name, $field_name))) {
    if (isset($form['#multigroups']) && isset($form['#multigroups'][$group_name][$field_name])) {
      $form_element = $form[$group_name][$delta][$field_name];
    }
    else {
      $form_element = $form[$group_name][$field_name][$delta];
    }
  }
  else {
    $form_element = $form[$field_name][$delta];
  }

  if (isset($form_element['_weight'])) {
    unset($form_element['_weight']);
  }

  $output = drupal_render($form_element);

  // AHAH is not being nice to us and doesn't know the "other" button (that is,
  // either "Upload" or "Delete") yet. Which in turn causes it not to attach
  // AHAH behaviours after replacing the element. So we need to tell it first.

  // Loop through the JS settings and find the settings needed for our buttons.
  $javascript = drupal_add_js(NULL, NULL);
  $filefield_ahah_settings = array();
  if (isset($javascript['setting'])) {
    foreach ($javascript['setting'] as $settings) {
      if (isset($settings['ahah'])) {
        foreach ($settings['ahah'] as $id => $ahah_settings) {
          if (strpos($id, 'filefield-upload') || strpos($id, 'filefield-remove')) {
            $filefield_ahah_settings[$id] = $ahah_settings;
          }
        }
      }
    }
  }

  // Add the AHAH settings needed for our new buttons.
  if (!empty($filefield_ahah_settings)) {
    $output .= '<script type="text/javascript">jQuery.extend(Drupal.settings.ahah, '. drupal_to_js($filefield_ahah_settings) .');</script>';
  }

  $output = theme('status_messages') . $output;

  // For some reason, file uploads don't like drupal_json() with its manual
  // setting of the text/javascript HTTP header. So use this one instead.
  $GLOBALS['devel_shutdown'] = FALSE;
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}
