<?php

/**
 * @file
 * Ad_ubercart administration routine.
 */

/**
 * Quota overview.
 */
function ad_ubercart_quota() {
  $result = pager_query(
    // Fantastic query, which returns ad plan's quota and usage... 
    'SELECT aq.*, daily_used, monthly_used FROM {ad_quota} aq
    LEFT JOIN (
      SELECT count(SUBSTR(date, 1, 7)) as "daily_used", date, type
        FROM {ad_used_quota}
          WHERE type = "daily"
            GROUP BY SUBSTR(date, 1, 7)) auq
      ON aq.month = SUBSTR(auq.date, 1, 7)
    LEFT JOIN (
      SELECT count(date) as "monthly_used", date, type
        FROM {ad_used_quota}
          WHERE type = "monthly"
            GROUP BY date) auq2
      ON aq.month = auq2.date
    ORDER BY aq.month DESC',
    // ...for last 12 months
      12, 0,
    // and this is COUNT query
    'SELECT count(*) FROM {ad_quota}');
  
  // Prepare a table.
  $header = array(t('Month'), t('Daily plans'), t('Monthly plans'), '');
  while ($quota = db_fetch_array($result)) {
    $daily_used = t('(Used: %used)', array('%used' => $quota['daily_used'] ? $quota['daily_used'] : 0));
    $monthly_used = t('(Used: %used)', array('%used' => $quota['monthly_used'] ? $quota['monthly_used'] : 0));
    $rows[] = array(
      ad_ubercart_format_month($quota['month']),
      $quota['daily'] .' '. $daily_used,
      $quota['monthly'] .' '. $monthly_used,
      l(t('edit'), 'admin/settings/ad_ubercart/quota/'. $quota['month'])
    );
  }
  
  if (!empty($rows)) {
    $output = theme('table', $header, $rows);
    $output .= theme('pager');
  }
  else {
    drupal_set_message(t('Please, <a href="!url">add</a> some quota first.', array('!url' => url('admin/settings/ad_ubercart/add_quota'))));
    $output = t('There is no available quota.');
  }
  
  return $output;
}

/**
 * Quota adding form.
 */
function ad_ubercart_quota_form($form_state, $active_month = NULL) {
  // Prepare a list of options if we have "add" form
  $months = get_next_12_months($active_month);
  $form['month'] = array(
    '#type' => 'select',
    '#options' => $months,
    '#requitred' => TRUE,
    '#title' => t('Month'),
  );
  
  // Load quota for specified month.
  if ($active_month) {
    $form['month']['#disabled'] = TRUE;
    $form['month']['#value'] = $active_month;
    
    // Also, get quota and usage amount.
    $quota = db_fetch_array(db_query('
      SELECT aq.*, daily_used, monthly_used FROM {ad_quota} aq
      LEFT JOIN (
        SELECT count(SUBSTR(date, 1, 7)) as "daily_used", date, type
          FROM {ad_used_quota}
            WHERE type = "daily"
              GROUP BY SUBSTR(date, 1, 7)) auq
        ON aq.month = SUBSTR(auq.date, 1, 7)
      LEFT JOIN (
        SELECT count(date) as "monthly_used", date, type
          FROM {ad_used_quota}
            WHERE type = "monthly"
              GROUP BY date) auq2
        ON aq.month = auq2.date
      WHERE aq.month = "%s"
      ORDER BY aq.month DESC', $active_month));
    $form['#quota'] = $quota;
  }
  else {
    // Default quota values.
    $quota['daily'] = 5;
    $quota['monthly'] = 25;
  }

  // Create rest of the form and fill it's defaults.
  $form['daily'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed daily plans per month'),
    '#requitred' => TRUE,
    '#default_value' => $quota['daily'],
    '#description' => $quota['daily_used'] ? format_plural($quota['daily_used'], '1 plan is already used.', '@count plans are already used, quota can not be less than this value.') : '',
  );
  $form['monthly'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed monthly plans per month'),
    '#requitred' => TRUE,
    '#default_value' => $quota['monthly'],
    '#description' => $quota['monthly_used'] ? format_plural($quota['monthly_used'], '1 plan is already used.', '@count plans are already used, quota can not be less than this value.') : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

/**
 * Returns list of next 12 months.
 */
function get_next_12_months($active_month) {
  // Add specified month to list only.
  if ($active_month) {
    $months[$active_month] = ad_ubercart_format_month($active_month);
  }
  // or 12 months if it's not specified
  else {
    $current_month = date('m');
    $current_year = date('Y');
    for ($h = 1; $h <= 12; $h++) {
      $month = $current_year .'-'. sprintf("%02d",$current_month);
      $months[$month] = ad_ubercart_format_month($month);

      $current_month++;
      if ($current_month > 13) {
        $current_year++;
        $current_month = 1;
      }
    }

    // Remove existing months from a list.
    $existing = get_quota();
    if (!empty($existing)) {
      foreach ($existing as $existing_month => $data) {
        unset($months[$existing_month]);
      }
    }
  }
  return $months;
}

/**
 * Quota form validator.
 */
function ad_ubercart_quota_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['daily']) || $form_state['values']['daily'] < 0) {
    form_set_error('daily', t('Should be a positive number or zero.'));
  }
  if (!is_numeric($form_state['values']['monthly']) || $form_state['values']['monthly'] < 0) {
    form_set_error('monthly', t('Should be a positive number or zero.'));
  }
  if (isset($form['#quota'])) {
    if ($form_state['values']['daily'] < $form['#quota']['daily_used']) {
      form_set_error('daily', t('Quota can not be less than already used.'));
    }
    if ($form_state['values']['monthly'] < $form['#quota']['monthly_used']) {
      form_set_error('monthly', t('Quota can not be less than already used.'));
    }
  }
}

/**
 * Quota form submit handler.
 */
function ad_ubercart_quota_form_submit($form, &$form_state) {
  if (isset($form['#quota'])) {
    drupal_write_record('ad_quota', $form_state['values'], array('month'));
    drupal_set_message(t('Quota for %month updated successfully!', array('%month' => ad_ubercart_format_month($form_state['values']['month']))));
  }
  else {
    drupal_write_record('ad_quota', $form_state['values']);
    drupal_set_message(t('Quota for %month added successfully!', array('%month' => ad_ubercart_format_month($form_state['values']['month']))));
  }
  
  $form_state['redirect'] = 'admin/settings/ad_ubercart';
}
