INSTALLATION

  1. Enable module at admin/build/modules.
  2. Ensure that you have two new content types: "Ad product: monthly" and
"Ad product: daily".
  3. Add advertisements quota at admin/settings/ad_ubercart/quota.
After quota was added, you should create at least two Ubercart's products. One
for monthly content type and one for daily. It doesn't matter what SKU, price
or anything thay will have, they should work fine, because they belongs to
proper product classes.