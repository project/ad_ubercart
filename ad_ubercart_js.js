Drupal.behaviors.ad_ubercart = function(context) {
  Drupal.settings.calendar.renderCallback = function($td, thisDate, month, year) {
    if (Drupal.settings.calendar.in_cart[thisDate.asString()]) {
      $td.addClass('used');
    }
    if (Drupal.settings.calendar.disabled_dates[thisDate.asString()]) {
      $td.addClass('disabled');
    }
    if ($td.is('.today')) {
      $td.addClass('disabled');
    }
  }
  
  $('.calendar:not(.adub-processed)', context).addClass('adub-processed').each(function() {
    include_calendar('.calendar', Drupal.settings.calendar, Drupal.settings.calendar.monthsToDisplay);
  });

  // Price estimates calculation
  $('.price-holder', context).hide();
  $('#edit-period:not(.adub-processed), input[@name=period]:not(.adub-processed)', context).addClass('adub-processed').change(function(){
    val = $(this).val();
    // DayPicker stores his values in hidden field as string,
    // so we need to split up it's values to get length
    if ($(this).is('input')) {
      if (val) {
        val = val.split(',');
      }
    }
    if (val) {
      price = val.length * $(this).attr('price');
      total = 'Estimated price: <b>$'+ price.toFixed(2) +'</b>';
      $('.price-holder', $(this).parents('form')).html(total).slideDown();
      
    }
    else {
      $('.price-holder', $(this).parents('form')).slideUp('fast',function(){ $(this).empty(); });
    }
  });
}

// Global calendar variables
var dates = new Array();
var calendars = new Array();

/**
 * Includes calendar on page
 * @param container
 *   Selector which points to DOM element where calendar will be placed.
 * @param value
 *   Selector to hidden value, where will be stored input.
 * @param settings
 *   Calendar settings object.
 * @param num
 *   Nymber of calendars to output.
 */
function include_calendar(container, settings, num) {
  // initialize calendar format
  Date.firstDayOfWeek = 7;
  Date.format = 'yyyy-mm-dd';
  
  // Don't touch this, it's dovumented initializer for datePicker
  for (var i = 0; i < num; i++) {
    calendars[i] = $('<div class="cal" index="'+i+'" />')
      .datePicker(settings)
      .bind(
        'dpMonthChanged',
        function(event, displayedMonth, displayedYear)
        {
          for (var i = 0; i < num; i++) {
            if (i != $(this).attr('index')) {
              $(".cal[index="+i+"]").dpSetDisplayedMonth(displayedMonth+(i-$(this).attr('index')), displayedYear);
            }
          }
        }
      )
      .bind(
        'dateSelected',
        function(event, date, $td, status)
        {
          for (var i = 0; i < num; i++) {
            if (i != $(this).attr('index')) {
              $(".cal[index="+i+"]").dpSetSelected(date.asString(), status, false);
            }
          }
          select_date(event, date, $td, status, $(this).parent(container).prev('input'));
        }
      );
    if ((num > 0) && (i < num-1)) {
      // remove the "forward" navigation from the first date picker
      $('.dp-nav-next', calendars[i]).html('');
    }
    if ((num > 0) && (i > 0)) {
      // remove the "backward" navigation from the first date picker
      $('.dp-nav-prev', calendars[i]).html('');
    }
  }
  
  // initialise the date pickers to consecutive months ($date1 defaults to this month so set $date2 to next month)
  var now = new Date();
  for (var i = 1; i < num; i++) {
    calendars[i].dpSetDisplayedMonth(now.getMonth()+i, now.getFullYear());
  }
  // add the generated combined plugin to the document
  $(container).html('');
  for (var i = 0; i < num; i++) {
    $(container).append(calendars[i]);
  }
}

// A way to sync multiple calendars
function select_date(e, date, $td, state, field) {
  if (state) {
    if (dates.indexOf(date.asString()) == -1) {
      dates.push(date.asString());
    }
  }
  else {
    pos = dates.indexOf(date.asString());
    if (pos >= 0) {
      dates.splice(pos,1);
    }
  }
  // Write our value to hidden field
  field.val(dates.toString());
  field.trigger('change');
}

// IE fix
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}
